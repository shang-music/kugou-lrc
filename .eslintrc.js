module.exports = {
  globals: {},
  extends: ['@s4p/eslint-config'],
  rules: {
    '@typescript-eslint/interface-name-prefix': ['error', 'never']
  },
};
